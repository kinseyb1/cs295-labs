<label name="recipe">Recipe Name</label><br>
<?php echo htmlentities($_POST["title"]);?>

<label>Ingredients</label>
<ul>
        <li><?php echo htmlentities($_POST["ing1"]);?></li>
        <li><?php echo htmlentities($_POST["ing2"]);?></li>
        <li><?php echo htmlentities($_POST["ing3"]);?></li>
</ul>

<label>Directions</label>
<?php echo htmlentities($_POST["directions"]);?>
<br />
<br />
<a href="index.php">Recipe List</a>
