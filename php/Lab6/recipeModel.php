<?php
require_once 'model.php';
require_once 'database.php';

class Recipe{
	public $id;
	public $title;
	public $ingredient0;
	public $ingredient1;
	public $ingredient2;
	public $instructions;
	
	function __construct($id, $title, $ingredient0, $ingredient1, $ingredient2, $instructions){
		$this->id = $id;
		$this->title = $title;
		$this->ingredient0 = $ingredient0;
		$this->ingredient1 = $ingredient1;
		$this->ingredient2 = $ingredient2;
		$this->instructions = $instructions;
	}
}

class RecipeModel extends Model{
	function findAll(){
		return results();
    }
	function addRecipe(){
		$title = htmlentities($_POST["title"]);
		$ingredient0 = htmlentities($_POST["ing1"]);
		$ingredient1 = htmlentities($_POST["ing2"]);
		$ingredient2 = htmlentities($_POST["ing3"]);
		$directions = htmlentities($_POST["directions"]);
		$recipe = new Recipe(null, $title, $ingredient0, $ingredient1, $ingredient2, $directions);
		insert($recipe);
	}
}