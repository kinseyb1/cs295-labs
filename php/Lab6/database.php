<?php
require_once 'recipeModel.php';
class Database{
	private static $conn = NULL;
	static function get_connection(){
		if (self::$conn == NULL){
			
			if (isset($_ENV['OPENSHIFT_APP_NAME'])){
				define("DB_NAME", "recipe");
				define("DB_HOST", $_ENV['OPENSHIFT_MYSQL_DB_HOST']);
				define("DB_USER", $_ENV['OPENSHIFT_MYSQL_DB_USERNAME']);
				define("DB_PASS", $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD']);
			}
			else{
				define("DB_NAME", "recipe");
				define("DB_HOST", "localhost");
				define("DB_USER", "root");
				define("DB_PASS", "");
				}
			self::$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			
		if (mysqli_connect_errno()){
			die("Are you able to cease thinking on command?  Connection failed.\nError Number ".mysqli_connect_errno);
			}
		}
	return self::$conn;
	}
}
function insert($recipe){
	$conn = Database::get_connection();
	$query = $conn->prepare("Insert into recipes(title, ingredient0, ingredient1, ingredient2, instructions) values (?, ?, ?, ?, ?)");
	$query->bind_param('sssss', $recipe->title, $recipe->ingredient0, $recipe->ingredient1, $recipe->ingredient2, $recipe->instructions);
	$query->execute();
}

function results(){
	$recipes = array();
	$conn = Database::get_connection();
	$query = $conn->prepare("select * from recipes");
	$query->execute();
	$query->bind_result($id, $title, $ingredient0, $ingredient1, $ingredient2, $instructions);
	while ($query->fetch()){
		$recipes[] = new Recipe($id, $title, $ingredient0, $ingredient1, $ingredient2, $instructions);
	}
	return $recipes;
}