<form action="index.php?action=insert" method="post">
					
	<fieldset>
					
		<legend>Please create a recipe</legend>
					
		<label name="recipe">Recipe Name</label>
			<input class="input-xxlarge" type="text" name="title">
					
		<label>Ingredients</label>
			<div><input class="input-xxlarge" type="text" name="ing1"></div>
			<div><input class="input-xxlarge" type="text" name="ing2"></div>
			<div><input class="input-xxlarge" type="text" name="ing3"></div>
			<div><input class="input-xxlarge" type="text" name="ing4"></div>
			<div><input class="input-xxlarge" type="text" name="ing5"></div>
					
		<label>Directions</label>
			<div><textarea name="directions" class="input-block-level" rows="10"></textarea></div>
					
		<button type="submit" class="btn-btn-primary">Submit</button>
					
	</fieldset>
</form>
<a href="index.php">Recipe List</a>