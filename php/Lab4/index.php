<?php
function render($page) {
    ob_start();
    require_once $page . '.php';
    $content = ob_get_clean();
    require_once 'layout.php';
}
if (($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_GET['action'])) && ($_GET['action'] == "insert")){
	render('Thanks');
}

elseif (($_SERVER['REQUEST_METHOD'] == "GET") && (isset($_GET['action'])) && ($_GET['action'] == "insert")){
	render('form');
}

else {
	render('list');
}
?>