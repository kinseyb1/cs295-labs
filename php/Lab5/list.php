<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Title</th>                  
            <th>Ingredient 0</th>
            <th>Ingredient 1</th>
            <th>Ingredient 2</th>
            <th>Instructions</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($this->data as $recipe) { ?>     
        <tr>
            <td><?php echo htmlentities($recipe->id); ?></td>
            <td><?php echo htmlentities($recipe->title); ?></td>                                
            <td><?php echo htmlentities($recipe->ingredient0); ?></td>
            <td><?php echo htmlentities($recipe->ingredient1); ?></td>
            <td><?php echo htmlentities($recipe->ingredient2); ?></td>
            <td><?php echo htmlentities($recipe->instructions); ?></td>
        </tr>                                
    <?php } ?>          
    </tbody>                
</table>     
<a href="index.php?action=insert">Insert a recipe</a>