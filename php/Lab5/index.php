<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once "view.php";
require_once "recipeModel.php";

if (($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_GET['action'])) && ($_GET['action'] == "insert")){
		$view = new View('thanks', null);
        $view->render();
}

elseif (($_SERVER['REQUEST_METHOD'] == "GET") && (isset($_GET['action'])) && ($_GET['action'] == "insert")){
		$view = new View('form', null);
        $view->render();
}

else {
		$recipeModel = new RecipeModel();
		$data = $recipeModel->findAll();
        $view = new View('list', $data);
        $view->render();
}
?>
