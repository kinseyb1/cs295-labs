<?php
class View{
	function __construct($page, $data){
		$this->page = $page;
		$this->data = $data;
	}
	function render() {
		ob_start();
		require_once $this->page . '.php';
		$content = ob_get_clean();
		require_once 'layout.php';
	}
}