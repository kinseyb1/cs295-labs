<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab2</title>
        <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/default.css" rel="stylesheet" media="screen">
	</head>
    <body>                
        <div class="container">
        	<h1 class="title">Awesome recipe site</h1>        
			<?php
				if ($_SERVER['REQUEST_METHOD'] == "GET"){
					echo <<<EOF
					<form action="index.php" method="post">
					
					<fieldset>
					
					<legend>Please create a recipe</legend>
					
						<label name="recipe">Recipe Name</label>
					<input class="input-xxlarge" type="text" name="title">
					
					<label>Ingredients</label>
					<div><input class="input-xxlarge" type="text" name="ing1"></div>
					<div><input class="input-xxlarge" type="text" name="ing2"></div>
					<div><input class="input-xxlarge" type="text" name="ing3"></div>
					<div><input class="input-xxlarge" type="text" name="ing4"></div>
					<div><input class="input-xxlarge" type="text" name="ing5"></div>
					
					<label>Directions</label>
					<div><textarea name="directions" class="input-block-level" rows="10"></textarea></div>
					
					<button type="submit" class="btn-btn-primary">Submit</button>
					
					</fieldset>
					</form>
EOF;
				}elseif ($_SERVER['REQUEST_METHOD'] == "POST"){
					$title = htmlentities($_POST["title"]);
					$ing1 = htmlentities($_POST["ing1"]);
					$ing2 = htmlentities($_POST["ing2"]);
					$ing3 = htmlentities($_POST["ing3"]);
					$ing4 = htmlentities($_POST["ing4"]);
					$ing5 = htmlentities($_POST["ing5"]);
					$directions = htmlentities($_POST["directions"]);
					echo <<<EOF
					<label name="recipe">Recipe Name</label><br>
					<b>$title</b><br>
					<label>Ingredients</label>
					<ul>
						<li>$ing1</li>
						<li>$ing2</li>
						<li>$ing3</li>
						<li>$ing4</li>
						<li>$ing5</li>
					</ul>
					<label>Directions</label><br>
					$directions
EOF;
				}else{
					echo "WRONG";
					}
			?>
		</div>       
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
    	<script src="../lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
